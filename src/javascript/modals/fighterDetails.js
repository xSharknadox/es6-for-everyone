import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name, attack, defense, health, source } = fighter;
  const attributes = { src: source };

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const imgElement = createElement({ tagName: 'img', className: 'fighter-image', attributes});
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  const healthElement = createElement({ tagName: 'span', className: 'fighter-health' });
  const attackElement = createElement({ tagName: 'span', className: 'fighter-attack' });
  const defenceElement = createElement({ tagName: 'span', className: 'fighter-defence' });
  
  nameElement.innerText = "Name: "+name+"; ";
  healthElement.innerText = "Health: "+health+"; ";
  attackElement.innerText = "Attack: "+attack+"; ";
  defenceElement.innerText = "Defence: "+defense+"; ";

  fighterDetails.append(imgElement, nameElement, healthElement, attackElement, defenceElement);

  return fighterDetails;
}
