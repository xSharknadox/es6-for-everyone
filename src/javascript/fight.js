export function fight(firstFighter, secondFighter) {
  let firstFighterHealth = firstFighter.health;
  let secondFighterHealth = secondFighter.health;

  let moveIndex = 1;
  while(firstFighterHealth>0 && secondFighterHealth>0){
    if(moveIndex%2===0){
      firstFighterHealth -= getDamage(secondFighter, firstFighter);
    }else{
      secondFighterHealth -= getDamage(firstFighter, secondFighter);
    }
  }

  if(firstFighterHealth>0){
    return firstFighter.name;
  }else if(secondFighterHealth>0){
    return secondFighter.name;
  }else{
    return 'Both died :)';
  }
}

export function getDamage(attacker, enemy) {
  const damage = getHitPower(attacker) - getBlockPower(enemy);
  return damage;
}

export function getHitPower(fighter) {
  const{attack} = fighter;
  const criticalHitChance = randomFloat(1,2);
  const power = attack*criticalHitChance;
  return power;
}

export function getBlockPower(fighter) {
  const{defense} = fighter;
  const dodgeChance = randomFloat(1,2);
  const power = defense*dodgeChance;
  return power;
}

function randomFloat(min, max) {
  return min + (max - min) * Math.random();
}
